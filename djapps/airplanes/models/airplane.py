import math
from decimal import Decimal

from django.db import models


class Airplane(models.Model):
    id = models.PositiveBigIntegerField(primary_key=True)
    number_of_passenger = models.PositiveIntegerField(blank=True, null=True, default=0)
    fuel_tank_capacity_multiplier = models.PositiveIntegerField(blank=True, null=True, default=200)
    base_consumption_multiplier = models.DecimalField(max_digits=17, decimal_places=2, blank=True, null=True, default=0.80)
    passenger_consumption_multiplier = models.DecimalField(max_digits=17, decimal_places=3, blank=True, null=True, default=0.002)

    def __str__(self):
        return f'Airplane #{self.id}'

    @property
    def full_tank_capacity_in_liters(self):
        return self.id * self.fuel_tank_capacity_multiplier

    @property
    def maximum_minutes_able_to_fly(self):
        max_minutes = self.full_tank_capacity_in_liters / self.fuel_consumption_per_minutes
        return math.floor(max_minutes)

    @property
    def fuel_consumption_per_minutes(self):
        fuel_consumption = self.base_fuel_consumption + self.passenger_fuel_consumption
        return Decimal("{:.3f}".format(round(fuel_consumption, 3)))

    @property
    def base_fuel_consumption(self):
        base_consumption = self.id * self.base_consumption_multiplier
        return Decimal("{:.2f}".format(round(base_consumption, 2)))

    @property
    def passenger_fuel_consumption(self):
        passenger_consumption = self.number_of_passenger * self.passenger_consumption_multiplier
        return Decimal("{:.3f}".format(round(passenger_consumption, 3)))
