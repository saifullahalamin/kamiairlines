from rest_framework.viewsets import ModelViewSet

from djapps.airplanes.models import Airplane
from djapps.airplanes.serializers.airplane import AirplaneSerializer


class AirplaneViewSet(ModelViewSet):
    serializer_class = AirplaneSerializer
    permission_classes = []

    def get_queryset(self):
        return Airplane.objects.all()
