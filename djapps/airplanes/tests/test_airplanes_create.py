from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient


class UserCreateAirplaneTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()
        cls.airplane_list_url = reverse('airplane-list')

        """
        Requirements
        We are creating a new software for a airline company called “KAMI Airlines”.
        - The company is assessing 10 different airplanes.
        - Each airplane has a fuel tank of (200 liters * id of the airplane) capacity. For example, if
        the airplane id = 2, the fuel tank capacity is 2*200 = 400 liters.
        - The airplane fuel consumption per minute is the logarithm of the airplane id multiplied by
        0.80 liters.
        - Each passenger will increase fuel consumption for additional 0.002 liters per minute.
        """
        cls.expected_response_data = [
            {
                'id': 2,
                'number_of_passenger': 10,
                'full_tank_capacity_in_liters': 400,  # id * 200 = 2 * 200 = 400
                'base_fuel_consumption': '1.60',  # id * 0.80 = 2 * 0.80 = 1.60
                'passenger_fuel_consumption': '0.020',  # number_of_passenger * 0.002 = 10 * 0.002 = 0.020
                'fuel_consumption_per_minutes': '1.620',  # base_fuel_consumption + passenger_fuel_consumption
                'maximum_minutes_able_to_fly': 246  # floor(full_tank_capacity_in_liters/fuel_consumption_per_minutes)
            },
            {
                'id': 3,
                'number_of_passenger': 5,
                'full_tank_capacity_in_liters': 600,  # id * 200 = 3 * 200 = 600
                'base_fuel_consumption': '2.40',  # id * 0.80 = 3 * 0.80 = 2.40
                'passenger_fuel_consumption': '0.010',  # number_of_passenger * 0.002 = 5 * 0.002 = 0.010
                'fuel_consumption_per_minutes': '2.410',  # base_fuel_consumption + passenger_fuel_consumption
                'maximum_minutes_able_to_fly': 248  # floor(full_tank_capacity_in_liters/fuel_consumption_per_minutes)
            },
        ]

    def test_airplane_with_given_id_and_calculation(self):
        for data in self.expected_response_data:
            response = self.client.post(self.airplane_list_url, data={
                'id': data['id'],
                'number_of_passenger': data['number_of_passenger']
            })

            # check status response
            self.assertEqual(response.status_code, status.HTTP_201_CREATED, response.data)

            # check expected calculation
            self.assertEqual(response.data['full_tank_capacity_in_liters'], data['full_tank_capacity_in_liters'])
            self.assertEqual(response.data['maximum_minutes_able_to_fly'], data['maximum_minutes_able_to_fly'])
            self.assertEqual(response.data['fuel_consumption_per_minutes'], data['fuel_consumption_per_minutes'])
            self.assertEqual(response.data['base_fuel_consumption'], data['base_fuel_consumption'])
            self.assertEqual(response.data['passenger_fuel_consumption'], data['passenger_fuel_consumption'])
