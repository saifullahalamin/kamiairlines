from django.contrib import admin

from djapps.airplanes.admin.airplane import AirplaneAdmin
from djapps.airplanes.models import Airplane

admin.site.register(Airplane, AirplaneAdmin)
