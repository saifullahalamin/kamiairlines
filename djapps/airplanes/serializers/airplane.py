from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from djapps.airplanes.models import Airplane


class AirplaneSerializer(ModelSerializer):
    fuel_consumption_per_minutes = serializers.DecimalField(max_digits=17, decimal_places=3, read_only=True)
    base_fuel_consumption = serializers.DecimalField(max_digits=17, decimal_places=2, read_only=True)
    passenger_fuel_consumption = serializers.DecimalField(max_digits=17, decimal_places=3, read_only=True)
    maximum_minutes_able_to_fly = serializers.IntegerField(read_only=True)

    class Meta:
        model = Airplane
        fields = [
            'id', 'number_of_passenger', 'full_tank_capacity_in_liters', 'maximum_minutes_able_to_fly',
            'fuel_consumption_per_minutes', 'base_fuel_consumption', 'passenger_fuel_consumption'
        ]
        read_only_fields = [
            'full_tank_capacity_in_liters', 'maximum_minutes_able_to_fly', 'fuel_consumption_per_minutes',
            'base_fuel_consumption', 'passenger_fuel_consumption'
        ]
