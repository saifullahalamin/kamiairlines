from django.contrib import admin

from djapps.authentication.admin.user import UserAdmin
from djapps.authentication.models import User

admin.site.register(User, UserAdmin)
